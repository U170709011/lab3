public class FindPrimes {

	public static void main(String[] args) {
		
		int maxNum = Integer.parseInt(args[0]);
		String str = "";
		
		for(int number=2;number<maxNum;number++) {
		  
			boolean isPrime=true;
		  
			for(int divisor=2;divisor<number;divisor++) {
		  
				if (number%divisor==0) {
		  
						isPrime=false; 
				} 
			}
			if (isPrime){
				str += number + ",";
		  
			}
		}	
		System.out.println((str.length()>1) ? str.substring(0, str.length()-1) : str);
		  
	}

}